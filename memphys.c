// SPDX-License-Identifier: GPL-2.0

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/uaccess.h>
#include <linux/io.h>
#include <linux/mm.h>

#define DEVICE_NAME "memphys"
#define CLASS_NAME  "memphys_class"

struct memphys_dev {
	struct cdev cdev;
	void * addr;
};

static dev_t dev_num;
static struct class *dev_class;
static struct memphys_dev memphys_device;

static unsigned long base_addr = 0;
module_param(base_addr, ulong, 0644);
MODULE_PARM_DESC(base_addr, "Physical base address of the memory region");

static unsigned long region_size = 0;
module_param(region_size, ulong, 0644);
MODULE_PARM_DESC(region_size, "Size of the memory region");

static int device_open(struct inode *inode, struct file *file)
{
	file->private_data = &memphys_device;
	return 0;
}

static int device_release(struct inode *inode, struct file *file)
{
	return 0;
}

static loff_t device_llseek(struct file *file, loff_t offset, int origin)
{
	return generic_file_llseek_size(file, offset, origin, region_size,
					region_size);
}

static ssize_t device_read(struct file *file, char __user *buffer, size_t length, loff_t *offset)
{
	struct memphys_dev *dev = file->private_data;
	uint8_t *data = (uint8_t *) (dev->addr + *offset);

	if (*offset > region_size) {
		return -EFAULT;
	}

	if (*offset + length > region_size) {
		length = region_size - *offset;
	}

	if (copy_to_user(buffer, data, length) != 0) {
		return -EFAULT;
	}
	else {
		(*offset) += length;
		return length;
	}
}

static ssize_t device_write(struct file *file, const char __user *buffer, size_t length, loff_t *offset)
{
	struct memphys_dev *dev = file->private_data;
	uint8_t *data = (uint8_t *) (dev->addr + *offset);

	if (*offset > region_size) {
		return -EFAULT;
	}

	if (*offset + length > region_size) {
		length = region_size - *offset;
	}

	if (copy_from_user(data, buffer, length) != 0) {
		return -EFAULT;
	} else {
		(*offset) += length;
		return length;
	}
}

static int device_mmap(struct file *file, struct vm_area_struct *vma)
{
	if (remap_pfn_range(vma, vma->vm_start,
						base_addr >> PAGE_SHIFT,
						vma->vm_end - vma->vm_start,
						vma->vm_page_prot)) {
		return -EAGAIN;
	}

	return 0;
}

static struct file_operations fops = {
	.owner = THIS_MODULE,
	.open = device_open,
	.release = device_release,
	.llseek = device_llseek,
	.mmap = device_mmap,
	.read = device_read,
	.write = device_write,
};

static int __init memphys_init(void)
{
	if (alloc_chrdev_region(&dev_num, 0, 1, DEVICE_NAME) < 0)
		return -1;
	if ((dev_class = class_create(THIS_MODULE, CLASS_NAME)) == NULL)
		goto undo_alloc_chrdev;
	if (device_create(dev_class, NULL, dev_num, NULL, DEVICE_NAME) == NULL)
		goto undo_class_create;
	cdev_init(&memphys_device.cdev, &fops);
	if (cdev_add(&memphys_device.cdev, dev_num, 1) == -1)
		goto undo_device_create;
	memphys_device.addr = memremap(base_addr, region_size, MEMREMAP_WB);
	return 0;

undo_device_create:
	device_destroy(dev_class, dev_num);
undo_class_create:
	class_destroy(dev_class);
undo_alloc_chrdev:
	unregister_chrdev_region(dev_num, 1);
	return -1;
}

static void __exit memphys_exit(void)
{
	memunmap(memphys_device.addr);
	cdev_del(&memphys_device.cdev);
	device_destroy(dev_class, dev_num);
	class_destroy(dev_class);
	unregister_chrdev_region(dev_num, 1);
}

module_init(memphys_init);
module_exit(memphys_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Jérôme Carretero <cJ-linux@zougloub.eu>");
MODULE_DESCRIPTION("A simple Linux char driver to access a portion of physical memory");
