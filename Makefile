# SPDX-License-Identifier: GPL-2.0

obj-m += memphys.o


ifndef KVER
 ifndef KDIR
KVER = $(shell uname -r)
KDIR = /lib/modules/$(KVER)/build
 else
KVER := $(shell (make -s -C $(KDIR) M=$(PWD) kernelrelease || make -s -C $(KDIR) M=$(PWD) kernelversion) | tail -n1)
ifeq ($(KVER),)
        $(error could not guess kernel version string from kernel sources at "$(KDIR)")
endif
 endif
endif
KDIR ?= /lib/modules/$(KVER)/build


all:
	make -C $(KDIR) M=$(PWD) modules

clean:
	make -C $(KDIR) M=$(PWD) clean

modules_install:
	make -C $(KDIR) M=$(PWD) modules_install
