#######
memphys
#######


This module provides a device file (`/dev/memphys`) allowing to access a range of physical memory.

This is basically acting as a subset of /dev/mem, which is really RAM.


Usage
#####

Example (using 1MiB of RAM @ 128GiB - 1MiB set aside by linux
by passing `mem=131071M` as kernel command-line argument):

.. code:: sh

   sudo modprobe memphys base_addr=0x1ffff00000 region_size=0x100000
   sudo chown $(id) /dev/memphys
